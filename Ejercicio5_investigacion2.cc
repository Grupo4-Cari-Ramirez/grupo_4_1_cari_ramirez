#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>


float med(float a, float b){
    float m = 0.0;
    m = b/a;

return m;
}

float var(int a[], int b, float c){
    int i;
    float suma;

    for (i=0; i<b; i++){
        suma+=((a[i]-c)*(a[i]-c));
    }

return (suma/b);
}


float desv(float a){
    float r;
    r = sqrt(a);
return r;
}

void frec(int a[], int b, int c[]){
    int i;
    for(i=0; i<b; i++){
        c[a[i]]++;
    }
}

int moda(int a[], int b){
    int i, M=0, C=a[0];

    for(i=1; i<b; i++){
        if(M < a[i]){
            M = i;
            C = a[i];
        }
    }
return (M);
}




int main(){
    float media, varia, desvi;
    int temp[100], f[15]={0};
    int N,s=0;
    int i, mod;

    printf("INGRESE N° DE ALUMNOS: " );
    scanf("%d",&N);
    printf("INGRESE NOTAS DEL 1 AL 7: \n\n");


    for (i=0; i<N; i++){
    printf("NOTA ALUMNO %d = ", i+1);
    scanf("%d",&temp[i]);
    s+=temp[i];
    }

    media = med(N,s);
    printf("La media aritmética es: %.2f \n",media);

    varia = var(temp,N,media);
    printf("La varianza es: %.2f \n",varia);

    desvi = desv(varia);
    printf("La desviación estándar es: %.2f \n",desvi);

    frec(temp, N, f);
    mod = moda(f, 15);
    printf("La moda es: %d\n", mod);
return 0;
}
