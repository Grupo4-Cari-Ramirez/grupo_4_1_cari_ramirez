//Programa 3
//Obtencion de datos a partir de una trama recibida.
#include <stdio.h>
#include <string.h>
#include <iostream>

//Definicion de variables
int main (){
char DATA[2400],Temp [100][2]={0},Hume[100][3]={0};
int i,j=0,f=0,count=0,count2=0;

//Obtenciond de la trama
printf("\n");
printf("DATA:");
scanf("%s", DATA);

//Separaciond de datos entre temperatura y humedad
for(i=0;i<strlen(DATA);i++){
    if(DATA[i] == '6' & DATA[i+1] == '0'){//Condicion para registrar Temperatura
        Temp[j][0]=DATA[i-2];
        Temp[j][1]=DATA[i-1];
    j++;
    count++; //Contador de numero de temperaturas registradas
    }
    if(DATA[i] == '8' & DATA[i+1] == '8'){//Condicion para registrar Humedad
        Hume[f][0]=DATA[i-3];
        Hume[f][1]=DATA[i-2];
        Hume[f][2]=DATA[i-1];
    f++;
    count2++;//Contador de numeor de humedad registrada
    }
}
//Presentacion de los datos registrados
printf("\n");
printf("Nº de temperaturas: %d    Nº de Humedades: %d \n",count,count2 );
printf("\n");
printf("Router 1 \t\t Router2 \n\tTemperatura:\t\t Humedad: \n");
//Presentacion de la matriz con los datos
for(i=0;i<count;i++){

    printf(" \ttemp[%d]=%c%cº\t\t Hum[%d]=%c%c%c \n",i+1, Temp[i][0] ,Temp[i][1],i+1,Hume[i][0],Hume[i][1],Hume[i][2]);

}
printf("\n");
return 0;
}
