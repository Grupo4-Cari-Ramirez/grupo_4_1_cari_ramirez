#include <stdio.h>
#include <stdlib.h>
#include <time.h>

float*getRandom (){
    static float temp[24];
    int i;

    srand48((unsigned) time( NULL ) );

    for (i=0; i<24; i++){
    temp[i]=drand48() * (40) -10; // rango de temperatura entre -10°C y 30°C
    }

return temp;
}

float promedio(){
    float *c;
    static float prom, s;
    int i;
    s=0;

    c=getRandom();
    for (i=0; i<24; i++){
    s = s + *(c+i);
    }

    prom = (s/24);

return prom;
}

int maxi(){
    static float *t=0;
    int i=0;
    float M;
    int I=0;

    t = getRandom();
    M = *(t+i);
    for (i=0; i<24; i++){
        if (M <= *(t+i)){
            M = *(t+i);
            I=i;
        }
    }
return I;
}

int mini(){
    static float *t=0;
    int i=0;
    float m;
    int I=0;

    t = getRandom();
    m = *(t+i);
    for (i=0; i<24; i++){
        if (m >= *(t+i)){
            m = *(t+i);
            I=i;
        }
    }
return I;
}

int main (){

    float *t=0;
    int i;
    int I=0;
    float S=0, P=0, prom;

    t=getRandom();
    for (i=0; i<24; i++){
    printf("Temperatura a las %d:00 Hrs = %.2f°C\n", i,*(t+i));
    }
    prom = promedio();
    printf("El promedio del día es de: %.2f°C\n", prom);
    I=maxi();
    printf("Se registra la máxima temperatura a las %d:00 Hrs. \n", I);

    I=mini();
    printf("Se registra la mínima temperatura a las %d:00 Hrs. \n", I);
    return 0;
}
