#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//#include <wiringPi.h>


int getRandom (int x[]){

    int i;

    srand((unsigned) time( NULL ) );

    for (i=0; i<256; i++){
    x[i]=rand() % 256; // Arreglo de largo 256
    printf("x[%d] = %d \n", i, x[i]);
    }

return 0;
}

int bin(int x, int B[]){
    int i, j, r, d;

    for(i=0; i<8; i++){
        r = x%2;
        x = x/2;
        B[i] = r;

        if (x == 1){
            B[i+1] = 1;
            break;
        }
    }

return 0;
}

int main(){
int d;
int a, b, i=0,I,x;
int o[255], c[8]={0};

    getRandom(o);

    printf("Ingrese una posición del arreglo = ");
    scanf("%d",&a);

    b = o[a];
    printf("La posición escogida contiene el número: %d\n", b);

    bin(b,c);

    printf("Y su formato binario es : \n");
    for (i=7; i>=0; i--){
    printf("%d ",c[i]);
    }
    printf("\n");

  /*  wiringPiSetup();
    for(I=7;I>=0;I--){
        x++;
        pinMode(x,OUTPUT);
        if(c[I]==1){
            digitalWrite(x, HIGH);
        }
        else{
            digitalWrite(x, LOW);
        }
    }*/


return 0;
}
