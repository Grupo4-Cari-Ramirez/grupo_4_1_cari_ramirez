import socket

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'


client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def send(M):
    msg = ''
    for a in M:
        if M == DISCONNECT_MESSAGE: # Reconocimiento para desconexión
            msg = M
            break
        if ord(a) == 32: #Mantener el espacio por comodidad
            msg += a
        elif ord(a) <= 77 and ord(a) >= 65: # Mayúsculas [A-M]
            msg += chr(ord(a) + 13)
        elif ord(a) <= 109 and ord(a) >= 97: # Minúsculas [a-m]
            msg += chr(ord(a) + 13)
        else:   # Las demás letras, minúsculas y mayúsculas
            msg += chr(ord(a) - 13)
    message=msg.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    client.send(send_length)
    client.send(message)
    print(msg)
    print(client.recv(2048).decode(FORMAT))

send('HOLA mundo')
input()
send('Hola Mundo')
input()
send(DISCONNECT_MESSAGE)

