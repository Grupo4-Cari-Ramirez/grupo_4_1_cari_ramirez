#include <stdio.h>

//Estructuras algoritmicas selectivas
//Empresa Textil # La Paz, Bolivia.

int main()
{
int CLA, CAT, ANT, RES;
printf("Bienvenido, aqui le informaremos si cuenta con las condiciones para desempeñarse en la sucursal \n");
//Obtencion de datos.
printf("Por favor, ingrese su clave de trabajador: ");
scanf("%d", &CLA);
printf("Indique su categoria: ");
scanf("%d", &CAT);
printf("Años de antigüedad: ");
scanf("%d", &ANT);

//Analisis y resolucion.

switch(CAT){
case 2:
	if (ANT >= 7)
		RES=1;
	else
		RES=0;
break;
case 3:
	if (ANT >= 5)
		RES=1;
	else
		RES=0;
break;
case 4:

	if (ANT >= 5)
		RES =1;
	else
		RES=0;
break;
default:
	 RES =0;
break;
}
//Mensaje para el usuario.

if (RES == 1)
	printf("\nTrabajador con clave %d, reune las condiciones para el puesto.\n ", CLA);

else
	printf("\nTrabador con clave %d, no reune las condiciones del puesto.\n ", CLA);

}
